#!/bin/bash

latest_data=$(curl -s "https://api.github.com/repos/codernr/band-page-generator/releases/latest")
download_url=$(egrep -oh -m 1 "(https://github.com/codernr/band-page-generator/releases/download/v[0-9]\.[0-9]\.[0-9]/v[0-9]\.[0-9]\.[0-9]\.tar\.gz)" <<< $latest_data)

curl -L $download_url --output band-page-genarator.tar.gz

tar -zxvf band-page-genarator.tar.gz

cd publish

dotnet BandPageGenerator.dll -o ../public/index.html -t ../public -s ../settings.json -d ../public/downloads